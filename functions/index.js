const functions = require('firebase-functions');
const admin = require('firebase-admin');


admin.initializeApp();


// exports.myfunction = functions.firestore.document("users/{user}/createdEvents/{doc}").onWrite((change, context) => {

//     console.log(change.after.data());

//     admin.firestore().collection(`Events`).doc(`${change.after.data().id}`).set({
//         name: change.after.data().name,
//         desc: change.after.data().desc,
//         id: change.after.data().id,
//         categories: change.after.data().categories,
//         vote: 0,
//         voteCount: change.after.data().voteCount
//     }, {merge: true});
    
// });

exports.userComment = functions.firestore.document("Events/{event}/Comments/{doc}").onWrite((change, context) => {
    
    console.log(change.after.data());
    
    admin.firestore().collection(`users/${change.after.data().uid}/Comments/`).doc(`${change.after.data().eventId}`).set({
        comment: change.after.data().comment
    });
    
});

exports.imageToken = functions.storage.object().onFinalize(async (object) => {
    await console.log(object);

    let name = object.name;
    let token = object.metadata.firebaseStorageDownloadTokens;
    let selfLink = object.selfLink;
    console.log(name.split('/')[1], "=======aposugiflkadjgipasfh");

    if(name.split('/')[0] === "Tenders") {
        if(name.split('/')[3] === 'MainImage.jpg') {
            admin.firestore().collection(`Tenders/`).doc(name.split('/')[2]).set({
                mainImageUrl: `https://firebasestorage.googleapis.com/v0/b/funding-project.appspot.com/o/${selfLink.split('/o/')[1]}?alt=media&token=${token}`
            }, {merge: true})
        } else {
            admin.firestore().collection(`Tenders/${name.split('/')[2]}/SideImageUrls/`).doc().set({
                url: `https://firebasestorage.googleapis.com/v0/b/funding-project.appspot.com/o/${selfLink.split('/o/')[1]}?alt=media&token=${token}`
            }, {merge: true})
        }
    } else {
        if(name.split('/')[2] === 'MainImage.jpg') {
            admin.firestore().collection(`Events/`).doc(name.split('/')[1]).set({
                mainImageUrl: `https://firebasestorage.googleapis.com/v0/b/funding-project.appspot.com/o/${selfLink.split('/o/')[1]}?alt=media&token=${token}`
            }, {merge: true})
        } else {
    
            admin.firestore().collection(`Events/${name.split('/')[1]}/SideImageUrls/`).doc().set({
                url: `https://firebasestorage.googleapis.com/v0/b/funding-project.appspot.com/o/${selfLink.split('/o/')[1]}?alt=media&token=${token}`
            }, {merge: true})
        }
    }
});