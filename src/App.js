import React from 'react';
import { SignUp } from './pages/sign-up';
import { SignIn } from './pages/sign-in';
import { HomeDefault } from './pages/home-default';
import { AddEvent } from './pages/add-event'
import { Verify } from './pages/verify';
import { EventSharing } from './pages/event-sharing';
import { TenderSharing } from './pages/tender-sharing';
import { Navigation } from './pages/navigation';
import { Components } from './pages/components';
import Profile from './pages/profile'
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import { AuthUserProvider } from './providers/auth-user-provider';
// import './style/grid.scss'
// import './style/grid-align.scss'
import './style/main.scss';
import { VoteHistory } from './pages/vote-history';

const App = () => {
    return (
        <AuthUserProvider>
            <Router>
                <Navigation>
                    <Switch>
                        <Route path="/login">
                            <SignIn />
                        </Route>
                        <Route path="/register">
                            <SignUp />
                        </Route>
                        <Route path="/addEvent">
                            <AddEvent />
                        </Route>
                        <Route path="/verify">
                            <Verify />
                        </Route>
                        <Route path="/event-id">
                            <EventSharing />
                        </Route>
                        <Route path="/tender-id">
                            <TenderSharing />
                        </Route>
                        <Route path="/profile">
                            <Profile />
                        </Route>
                        <Route path="/vote-history">
                            <VoteHistory />
                        </Route>
                        <Route path="/forgetpass">
                            <HomeDefault />
                        </Route>
                        <Route path="/components">
                            <Components />
                        </Route>
                        <Route path="/">
                            <HomeDefault />
                        </Route>
                    </Switch>
                </Navigation>
            </Router>
        </AuthUserProvider>
    )
}

export default App
