import React, { useEffect, useContext, useRef, useState } from 'react';
import { useDoc, useCol, useFirebase } from '../Hooks/firebase';
import { AuthContext } from '../providers/auth-user-provider';
// import { Button } from '../components/button'
import { useStorage } from '../Hooks';
import { Button } from '../components';
import { useHistory } from 'react-router-dom';
import { Stack } from '../components'
import { RenderProfilePost } from '../components/render-profile-post';

const Profile = (vote, voteCount) => {
    const { user } = useContext(AuthContext)
    const { uid } = user || {};
    const history = useHistory();
    const { data, updateRecord } = useDoc(`users/${uid}`);
    const { data: createdPosts } = useCol(`users/${uid}/createdEvents`);
    const { data: allEventInfo } = useCol(`Events`)
    const { profileImage, username } = data || {};
    const [file, setFile] = useState('')
    const [imgSrc, setImgSrc] = useState('')
    const inputFile = useRef(null);
    const { firebase } = useFirebase();
    const profileImageSrc = useStorage(`profileImages/${uid}/profileImage.jpg`)

    useEffect(() => {
        if (inputFile) {
            function onFileChange() {
                setFile(inputFile.current.files[0]);
                console.log(inputFile)
                setImgSrc(URL.createObjectURL(inputFile.current.files[0]))
            }

            inputFile.current.addEventListener('change', onFileChange);
        }
    }, [inputFile]);

    useEffect(() => {
        if (firebase && file && uid) {
            console.log("aafd")
            var storageSideRef = firebase.storage().ref().child(`profileImages/${uid}/profileImage.jpg`);
            storageSideRef.put(inputFile.current.files[0])
                .then((snapshot) => {
                    console.log('Done. Storage');
                    updateRecord({ profileImage: "img" })
                    console.log('Done. Firestore')
                })
        }
    }, [firebase, file, uid, imgSrc, updateRecord])

    // let a = allEventInfo && allEventInfo.map((e) => {
    //     return createdPosts.filter((j) => {
    //         if(j.id === e.id) {
    //             return e;
    //         }
    //     })
    // });fdskgdfksdjfsdkfjhdskjfhasdasdasdasd
    // console.log(a);
    // useEffect(() => {

    // }, [createdPosts, allEventInfo])


    return (
        <div className='container h-vh-100 tempCol-0-4'>

            <div className="flex flex-col">
                <Stack size={5}>
                    <div style={{ borderRadius: '100%', backgroundColor: '#9C9C9C', backgroundSize: 'cover', backgroundImage: `url("${imgSrc === '' ? profileImageSrc : imgSrc}")` }} className=' bradius-10 w-100 h-100 margin-auto' >
                        <p className='op'>{(profileImage === "default") && (imgSrc === '') ? username[0] : ''}</p>
                    </div>
                    <div className="flex-center">
                        <h3 className=" m-10 mr-20 ml-20 font-DmSans">{(data && data.name) || 'Abraham Linkoln'}</h3>
                    </div>

                    <input type='file' id='file' ref={inputFile} style={{ display: 'none' }} />


                    <div>
                        <Button onClick={() => { inputFile.current.click() }} className="bradius-5 fs-12 btn w80"> Edit profile</Button>
                    </div>
                    <div className="flex-row justify-between">
                        <button className=" bradius-5 normal fs-12 h-25 w-vw-27 btn-pf"> Gmail</button>
                        <button className=" bradius-5 normal fs-12 h-25 w-vw-27 btn-pf"> Call</button>
                        <button onClick={() => { history.push('/vote-history') }} className=" bradius-5 normal fs-12 h-25 w-vw-27 btn-pf">History</button>
                    </div>
                    <div className='w100 h-1 b-loading'></div>
                </Stack>
                <div>
                    <div className="mt-30 fs-24 fw-700 font-DmSans">Your Posts</div>
                    {
                        allEventInfo && allEventInfo.filter((event) => !createdPosts.every((post) => post.id !== event.id)).map((e) => <RenderProfilePost key={e.id} {...e}/>)
                    }
                </div>
            </div>
        </div>
    )
}

//DOING THIS -Sonor :^)

export default Profile