import React, { useState, useEffect, useContext } from 'react'
import { useDoc, useCol } from '../Hooks/firebase';
import { FacebookSharing } from '../components/facebook';
import { FormInput } from '../components/form-input';
import { AuthContext } from '../providers/auth-user-provider'
import { useKeyPress } from '../Hooks/key-press'
import { Button } from '../components';
import { useHistory } from 'react-router-dom'
import { LeftIcon, RightIcon } from '../components'



export const TenderSharing = () => {
    let url = new URLSearchParams(window.location.search).get('id');
    const { user } = useContext(AuthContext);
    const history = useHistory();
    const [userLocal, setUserLocal] = useState(null);

    const { data } = useDoc(`Tenders/${url}`);

    const { createRecord: createTenderComments, data: tenderComments, loading: commentLoading } = useCol(`Tenders/${url}/Comments`);
    const { data: sideImageUrls, loading: imageLoading } = useCol(`Tenders/${url}/SideImageUrls`);
    const [indx, setIndx] = useState(0);
    const [allImageUrls, setAllImageUrls] = useState([]);
    const [showComments, setShowComment] = useState(false);



    useEffect(() => {
        if (sideImageUrls && data) {
            setAllImageUrls([{ url: data.mainImageUrl }, ...sideImageUrls])
        }

    }, [data, sideImageUrls])


    const submit = () => {
        const id = randomStringAndNumber();
        if (comment !== '') {
            createTenderComments(userLocal.uid + "--" + id, {
                comment: comment,
                username: data2.username,
                uid: userLocal.uid,
                eventId: url
            });
        }
        setComment('')
    }

    useKeyPress("Enter", submit);

    const isLoaded = (...loaders) => loaders.reduce((condition, loader) => condition || loader, false);

    useEffect(() => {
        if (user) {
            setUserLocal(user);
        }
    }, [user]);

    const { data: data2 } = useDoc(`users/${userLocal && userLocal.uid}`);


    const [comment, setComment] = useState('');
    const handleChangeComment = (e) => setComment(e.target.value);

    const randomStringAndNumber = () => {
        let result = '';
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < 7; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    if (isLoaded(commentLoading, imageLoading)) {
        return (
            <div className='flex-center h-vh-80 bold'>
                <h3>Loading ...</h3>
                <div className="loader"></div>
            </div>
        )
    }


    return (
        <div className="font-Raleway">
            {/* <div style={{ backgroundColor: 'gray', backgroundSize: 'cover', backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundImage: `url("${data && data.mainImageUrl}")` }} className='w100 h30' ></div> */}
            <div style={{ backgroundColor: 'gray', backgroundSize: 'cover', backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundImage: `url("${allImageUrls && allImageUrls[indx].url}")` }} className='w100 h30 flex justify-between items-center pa-10'>
                {
                    indx !== 0 ? <LeftIcon width={30} height={30} onClick={() => { setIndx((indx) => indx - 1); console.log(allImageUrls[indx].url) }} />
                        : <div></div>
                }
                {
                    (allImageUrls && indx !== allImageUrls.length - 1) ? <RightIcon width={30} height={30} onClick={() => { setIndx((indx) => indx + 1); console.log(allImageUrls[indx].url) }} />
                        : <div></div>
                }
            </div>
            <div className="margin-auto w-vw-90">
                <div className='flex-row items-center justify-between'>
                    <p className='fs-18'>Tender Bill</p>
                    <h3 >{data && data.funds}$</h3>
                </div>

                <div>
                    <div>
                        <p>Started on: 2020.10.25</p>
                        <b>Ends on: 2020.11.12</b>
                    </div>
                </div>

                <div className='w100 mt-30 h-1 b-loading'></div>

                <h1>{data && data.name}</h1>
                <p className='wbreak'>{data && data.desc}</p>

                <div className='w100 mt-30 h-1 b-loading'></div>

                <div className='w-vw-90 h-50 bottom-7 margin-auto p-f z-i'>

                    {
                        (JSON.stringify(data && data.createdUser) === JSON.stringify(user && user.uid)) ?
                            <button disabled className='w100 h100 b-gray2 rb bradius-5'>
                                <p className="c-default bold">My Tender</p>
                            </button>
                            :
                            <Button className='w100 h100 b-primary rb bradius-5' onClick={() => { history.push(`/addEvent?tender=${url}`) }}>
                                <p className="c-default bold">Оролцох</p>
                            </Button>
                    }


                </div>


                <FacebookSharing></FacebookSharing>


                {/* <h3>Categroies</h3>
                <ul>
                    {
                        data && data.categories && data.categories.map((e, i) => {
                            return (
                                <li key={i}>{e}</li>
                            )
                        })
                    }
                </ul> */}
                <div className="w100">
                    <h1>Location</h1>
                    <p className="wbreak">{data && data.location}</p>
                </div>
                {/* design deer bvl hiigeerei = oroltsoj bui eventuudiig harah */}
                {/* <div className='w100 flex justify-end'>
                    <p onClick={() => {history.push(`/event-id?id=${data && data.tender}`)}}>oroltsoj bui eventuudiig harah</p>
                </div> */}
                <div className='w100 h-1 b-loading mt-30'></div>

                <h3>Comments</h3>

                <FormInput onChange={handleChangeComment} className="pa-7 b-gray6 rb outl bradius-10" value={comment} placeholder="Сэтгэгдэл үлдээх..."></FormInput>

                {/* <div>
                    {
                        tenderComments && tenderComments.map((e, i) => {
                            return (
                                <div key={i} className="mt-10 mb-20">
                                    <h4 className="ma-0">{e.username}</h4>
                                    <p className="ma-0 break-wrap">{e.comment}</p>
                                </div>
                            )
                        })
                    }
                </div> */}
                <div>
                    {
                        tenderComments && tenderComments.map((e, i) => {
                            if (i < 2) {
                                return (
                                    <div key={i} className="mt-10 mb-20">
                                        <h4 className="ma-0">{e.username}</h4>
                                        <p className="ma-0 break-wrap">{e.comment}</p>
                                    </div>
                                )
                            }
                            return '';
                        })
                    }
                    {
                        (showComments) ?
                            tenderComments && tenderComments.map((e, i) => {
                                if (i >= 2) {
                                    return (
                                        <div key={i} className="mt-10 mb-20">
                                            <h4 className="ma-0">{e.username}</h4>
                                            <p className="ma-0 break-wrap">{e.comment}</p>
                                        </div>
                                    )
                                }
                                return '';
                            })
                            :
                            tenderComments && tenderComments.length > 2 ?
                                <div className='w100 justify-center flex'>
                                    <div className="mt-10 mb-20 ul c-loading" onClick={() => { setShowComment(true) }}>See all comments</div>
                                </div>
                                :
                                <></>
                    }
                </div>
            </div>

            <div className="h-50 v-vw-100"></div>

        </div>
    )
}