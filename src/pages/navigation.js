import React, { useEffect, useState } from 'react'
import { DropdownMenu, ArrowIcon, XIcon } from '../components/';
import { useHistory } from 'react-router-dom';

export const Navigation = ({ children }) => {
    let history = useHistory();
    let [checked, setChecked] = useState('home');

    useEffect(() => {

        if (history) {
            console.log(history.location.pathname)
            if (history.location.pathname === '/profile') {
                setChecked('profile');  
            }
            if (history.location.pathname === '/register') {
                setChecked('register');
            }
            if (history.location.pathname === '/forgetpass') {
                setChecked('forget');
            }
            if (history.location.pathname === '/search') {
                setChecked('search');
            }
            if (history.location.pathname === '/event-id') {
                setChecked('event-id');
            }

        }
    }, [history, children])

    if (checked === "profile") {
        return (
            <div>
                <div className='pa-10 pr flex items-center'>
                    <ArrowIcon onClick={() => { history.goBack() }} width={12} height={12} />
                    <h3 className="ml-15">Profile</h3>
                </div>

                {children}
            </div>
        )
    } else if (checked === "register") {
        return (
            <div>
                <div className='pr flex items-center justify-end mr-10'>
                    <p>Skip</p>
                </div>
                {children}
            </div>
        )
    } else if (checked === "forget") {
        return (
            <div>
                <div className='pr flex items-center justify-end ma-10'>
                    <XIcon width={15} height={15} />
                </div>
                {children}
            </div>
        )
    } else if (checked === "search") {
        return (
            <div>
                <div className='pa-10 pr flex items-center justify-between'>
                    <ArrowIcon onClick={() => { history.goBack() }} width={20} height={20} />
                    <h3 className="font-comfortaa">logo</h3>
                    <DropdownMenu ></DropdownMenu>
                </div>
                {children}
            </div>
        )
    } else if (checked === "event-id") {
        return (
            <div>
                <div className='pa-10 pr flex items-center justify-between'>
                    <ArrowIcon onClick={() => { history.goBack() }} width={20} height={20} />
                    <h3 className="font-comfortaa">logo</h3>
                    <div>{` `}</div>
                </div>
                {children}
            </div>
        )
    } else if (checked === "") {
        return (
            <div>
                <div className='pr flex items-center justify-between margin-auto w90'>
                    <h3 className="font-Raleway">logo</h3>
                    <DropdownMenu ></DropdownMenu>
                </div>
                {children}
            </div>
        )
    }
    return (
        <div>
            <div className='pr flex items-center justify-between margin-auto w90 z-2'>
                <h3 className="font-glegoo" onClick={() => { history.push("/") }}>Chingis</h3>
                <DropdownMenu ></DropdownMenu>
            </div>
            {children}
        </div>
    )
}