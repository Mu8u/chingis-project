import React, { useState, useContext, useRef, useEffect } from 'react';
import { FormInput } from '../components';
import { useCol, useDoc } from '../Hooks/firebase';
import { AuthContext } from '../providers/auth-user-provider'
import { useFirebase } from '../Hooks/firebase';
// import { CameraIcon } from '../components/icons'
import { useHistory } from 'react-router-dom'
import { SearchInput } from '../components/search-input'
import { CameraIcon } from '../components/icons/camera-icon'
import '../style/main.scss'
import { XCircleIcon } from '../components/icons/x-circle-icon';
import { useStorage } from '../Hooks';
// import { from } from 'rxjs';
// import { ComplexInnerSubscriber } from 'rxjs/internal/innerSubscribe';

export const AddEvent = () => {


    let checkTender = new URLSearchParams(window.location.search).get('tender');
    // console.log(checkTender)
    const { user } = useContext(AuthContext)
    const history = useHistory();
    const [location, setLocation] = useState('');
    const [eventName, setEventName] = useState('');
    const [desc, setDesc] = useState('');
    const [file, setFile] = useState('');
    const [voteCount, setVoteCount] = useState('');
    const [images, setImages] = useState([]);
    const [imageSrc, setImageSrc] = useState('');
    const [sideImageSrc, setSideImageSrc] = useState('');
    const [funds, setFunds] = useState('');
    const [search, setSearch] = useState('');
    const { firebase } = useFirebase();
    const inputFile = useRef(null);
    const inputSideFile = useRef(null);

    const isLoaded = (...loaders) => loaders.reduce((condition, loader) => condition || loader, false);

    let uid;

    if (user != null) {
        uid = user.uid
    }

    const { createRecord } = useCol(`/Events/`);
    const { createRecord: createTenderEvent } = useCol(`Tenders/${checkTender}/Events`)
    const { createRecord: createTender } = useCol(`/Tenders/`);
    const { data: tenderInfo } = useDoc(`/Tenders/${checkTender && checkTender}`);
    const { data: logged, loading: userLoading, } = useDoc(`/users/${uid}`);
    const { createRecord: createCreateTender } = useCol(`/users/${uid}/createdTenders`);
    const { createRecord: createCreateEvent } = useCol(`/users/${uid}/createdEvents`);
    const { updateRecord } = useCol(`/Categories`);
    const { data } = useCol(`/Categories`);
    const [tags, setTags] = useState({});

    const handleChangeLocation = (e) => setLocation(e.target.value);
    const handleChangeEventName = (e) => setEventName(e.target.value);
    const handleChangeDesc = (e) => setDesc(e.target.value);
    const handleChangeVoteCount = (e) => setVoteCount(e.target.value);
    const handleChangeSearch = (e) => setSearch(e.target.value);
    const handleChangeFunds = (e) => setFunds(e.target.value);

    const clearSearch = () => {
        setSearch('')
    }

    const randomStringAndNumber = () => {
        let result = '';
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < 7; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    const Add = () => {
        let id = randomStringAndNumber();
        let searches = search.split(' ');
        let cate = [];

        searches.map((e) => {
            return data.map((g) => {
                if (e === g.category) {
                    cate.push(e);
                }
                return e;
            })
        })


        data.map((e) => {
            return cate.map((g) => {
                if (e.category === g) {
                    updateRecord(e.id, { voted: (Number(e.voted) + 1) })
                }
                return g;
            });
        })


        if (logged.logged !== "Group") {
            // let mainImageUrl = `EventImages/${id}/MainImage.jpg`
            // if(checkTender) {
            //     mainImageUrl = `Tenders/Events/${id}/MainImage.jpg`
            // }
            if (file) {
                var storageRef2 = firebase.storage().ref().child(`EventImages/${id}/MainImage.jpg`);;
                storageRef2.put(file)
                    .then((snapshot) => {
                        console.log('Done.');
                    })
                images.map((e, i) => {
                    var storageSideRef = firebase.storage().ref().child(`EventImages/${id}/SideImage-${i}.jpg`);;
                    storageSideRef.put(e)
                        .then((snapshot) => {
                            console.log('Done. SideImage');
                        })
                    return storageSideRef;
                })
            }

            createRecord(id, { name: eventName, desc: desc, location: location, id: id, categories: cate, tender: checkTender && checkTender, createdUser: user.uid, vote: 0, voteCount: Number(tenderInfo ? tenderInfo.voteCount : voteCount), createdAt: firebase.firestore.FieldValue.serverTimestamp() });
            createCreateEvent(id, { id: id })
            checkTender && checkTender ? createTenderEvent(id, { id: id }) : console.log('not tender');
        } else {
            let tid = randomStringAndNumber();
            if (file) {
                console.log('aaad')
                var storageRef = firebase.storage().ref().child(`Tenders/Events/${tid}/MainImage.jpg`);;
                storageRef.put(file)
                    .then((snapshot) => {
                        console.log('Done.');
                    })
                images.map((e, i) => {
                    var storageSideRef = firebase.storage().ref().child(`Tenders/Events/${tid}/SideImage-${i}.jpg`);;
                    storageSideRef.put(e)
                        .then((snapshot) => {
                            console.log('Done. SideImage');
                        })
                    return storageSideRef;
                })
            }

            createTender(tid, { name: eventName, desc: desc, location: location, tenderId: tid, categories: cate, createdUser: user.uid, vote: 0, voteCount: Number(voteCount), funds: funds, createdAt: firebase.firestore.FieldValue.serverTimestamp() });
            createCreateTender(id, { id: id })
        }

        history.push("/")
    }

    const remove = (tag) => {
        const filteredTags = Object.keys(tags).filter((obj) => obj !== tag);
        if (filteredTags.length === 0)
            setTags({});
        setTags(filteredTags.reduce((res, field) => ({...res, [field]: field}), {}))
    }

    // useEffect(() => {
    //     console.log(inputFile)
    //     if (inputFile.current) {

    //         console.log(inputFile)
    //         console.log('aa')

    const onFileChange = () => {
        setFile(inputFile.current.files[0]);
        setImageSrc(URL.createObjectURL(inputFile.current.files[0]))
    }

    const onSideFileChange = () => {
        setFile(inputFile.current.files[0]);
        setSideImageSrc(URL.createObjectURL(inputSideFile.current.files[0]))
    }

    //         inputFile.current.addEventListener('change', onFileChange);

    //     }
    // }, [inputFile]);

    useEffect(() => {
        if (inputSideFile.current) {

            function onSideFileChange() {

                const sideImages = Object.keys(inputSideFile.current.files).filter(key => key !== 'length').map(key => inputSideFile.current.files[key]);
                setImages(sideImages);
            }

            inputSideFile.current.addEventListener('change', onSideFileChange);
        }
    }, [inputSideFile]);

    // useEffect(() => {
    //     if (firestore) {
    //         var cat = firestore.collection('Categories');f
    //         cat.orderBy('voted', 'desc').limit(3).onSnapshot((querySnapshot) => {
    //             setSrt(querySnapshot.docs.map((doc) => doc.data()))
    //         })
    //     }
    // }, [firestore]);

    // useEffect(() => console.log(tags), [tags])

    if (isLoaded(userLoading)) {
        return (
            <div className='flex-center h-vh-80 bold'>
                <h3>Loading ...</h3>
                <div className="loader"></div>
            </div>
        )
    }

    console.log(tags)
    return (
        <div className="font-Raleway w-vw-100 margin-auto">
            <div className="ws90 flex margin-auto justify-end">
                <div className="font-Raleway bold ib fs-21 ws33 text-center">Аян Нэмэх</div>
                {/* example of using button component */}
                {
                    (logged && logged.logged === "Group")
                        ?
                        <div className="font-Raleway bold ib fs-18 ws33 text-end" onClick={Add} style={{ cursor: 'pointer' }} >Create Tender</div>
                        :
                        <div className="font-Raleway ws33 text-end mb-40 bold fs-18" onClick={Add} style={{ cursor: 'pointer' }} >{checkTender ? "Join Tender" : "Нийтлэх"}</div>
                }

            </div>


            {/* </form> */}
            <div id='form'>
                <div className="h-1 w-vw-100 b-gray4 " ></div>
                <FormInput type="event" className="input rb font-Raleway fs-18 lh-18" placeholder='Гарчиг нэмэх' value={eventName} onChange={handleChangeEventName} />
                <div className="h-1 w-vw-100 b-gray4 " ></div>
                <FormInput type="event" className="input rb font-Raleway fs-18 h-vh-20" placeholder='Тайлбар үүсгэх' value={desc} onChange={handleChangeDesc} />
                <div className="h-1 w-vw-100  b-gray4" ></div>
                <FormInput type="event" className="input rb font-Raleway fs-18" placeholder='Байршил нэмэх' value={location} onChange={handleChangeLocation} />
            </div>

            <div className="h-1 w-vw-100  fs-18 lh-23 b-gray4" ></div>
            {
                (logged && logged.logged === "Group") ?
                    <div>
                        <FormInput placeholder='Funds' value={funds} onChange={handleChangeFunds} />
                    </div>
                    :
                    <div></div>
            }


            {/* add category  */}
            <div id="category" className="w-vw-90 margin-auto">
                <div className="flex flex-row justify-between fs-18 lh-23 m-15 font-Raleway">
                    <div>Ангилал</div>
                    <div>{Object.values(tags).length}/3</div>
                </div>
                <SearchInput className="mb-10" updateSearch={setSearch} addTag={(tag) => {
                    if (Object.values(tags).length <= 2) setTags((tags) => ({ ...tags, [tag]: tag }))
                }} />
                {
                    tags && Object.values(tags).map((tag, index) =>
                        <div className="ib ms" key={index}>
                            <div className="pa-10 b-gray4 bradius-10 w-fit flex-row">
                                <div className=" ">{tag}</div>
                                <XCircleIcon className=' closeIcon ml-10' onClick={() => remove(tag)} width={25} height={25} />
                            </div>
                        </div>
                    )
                }


            </div>

            <div className="h-1 w-vw-100 mline fs-18 lh-23  b-gray4" ></div>

            {/* number of vote */}
            <div id="number of vote" className="w-vw-90 margin-auto">
                {
                    checkTender
                        ?
                        <div className="font-Raleway mb-24 fs-18">handiviin dun l gesen batalgaagu bga</div>
                        :
                        <div className="font-Raleway mb-24 fs-18">Саналын дүн</div>
                }
                {
                    tenderInfo ?
                        logged.logged === 'Group' ?
                            <FormInput type="number" className="input bradius-10 b-category rb ph-12 pa-12 fs-16 font-Raleway" placeholder="number of vote" value={tenderInfo.voteCount} disabled></FormInput>
                            :
                            <FormInput type="number" className="input bradius-10 b-category rb ph-12 pa-12 fs-16 font-Raleway" placeholder="Саналын дүн" value={voteCount} onChange={handleChangeVoteCount}></FormInput>
                        :
                        <FormInput type="number" className="input bradius-10 b-category rb ph-12 pa-12 fs-16 font-Raleway" placeholder="number of vote" value={voteCount} onChange={handleChangeVoteCount}></FormInput>
                }
            </div>

            <div className="h-1 w-vw-100 mline fs-18 lh-23  b-gray4" ></div>

            {/* date */}
            <div id="date" className="w-vw-90 margin-auto">
                <div className="font-Raleway fs-18 mb-24">Он сар нэмэх</div>
                <FormInput type="date" className="w-vw-90 rb b-gray4 font-Raleway bradius-10 pa-8"></FormInput>
            </div>

            <div className="h-1 w-vw-100 mline fs-18 lh-23 b-gray4" ></div>


            <div id='take-photo' className="w-vw-90 margin-auto">
                {/* zurag avna */}
                <div className="font-Raleway fs-18 mb-23">Зураг нэмэх</div>

                <input onChange={() => onFileChange(0)} type='file' id='file' ref={inputFile} style={{ display: 'none' }} />

                <div className="h-vh-35 ws-100 flex-center b-gray2 " style={{ backgroundImage: `url("${imageSrc}")`, backgroundSize: 'cover' }}>
                    <CameraIcon height={30} width={30} onClick={() => { inputFile.current.click() }} />

                    {/* <Button onClick={() => { inputFile.current.click() }}>
                        <CameraIcon height={30} width={30} />
                    </Button> */}
                    {/* <img id='img' src={imageSrc} className='ws-100 h-100' alt='img'></img> */}
                </div>
                {/* add multiple file */}
                <div id='take-side-images' className="mb-20">
                    <input onChange={onSideFileChange} type='file' id='sidefile' ref={inputSideFile} style={{ display: 'none' }} multiple />
                    <div className="flex flex-row justify-between">
                        <div className="b-gray2 b-size-cover h-vh-13 w-vw-28 flex flex-center flex-row mt-10">
                            {/* <Button className="flex align-end justify-end" onClick={() => { inputSideFile.current.click() }}></Button> */}
                            <CameraIcon height={20} width={20} onClick={() => { inputSideFile.current.click() }} />
                        </div>
                        <div className="b-gray2 h-vh-13 w-vw-28 flex flex-center flex-row mt-10">
                            <CameraIcon height={20} width={20} onClick={() => { inputSideFile.current.click() }} />
                        </div>
                        <div className="b-gray2 h-vh-13 w-vw-28 flex flex-center flex-row mt-10">
                            <CameraIcon height={20} width={20} onClick={() => { inputSideFile.current.click() }} />
                        </div>
                    </div>
                </div>
                {/* {
                    // console.log(sideImages)
                    images && images.map((e, i) => {
                        console.log(e)
                        return (
                            <div src={URL.createObjectURL(e)} key={i} className='ws-100 h-100' alt='img'></div>
                        )
                    })
                } */}
            </div>
        </div>
    )
}
