import React, { useState, useContext } from 'react';
import { SearchInput, TenderCampaign, ShowTender, Button } from '../components'
import { ShowPosts } from '../components/showposts'
import { useCol, useDoc, useFirebase } from '../Hooks/firebase';
import { AuthContext } from '../providers/auth-user-provider'


export const HomeDefault = () => {
    const { user } = useContext(AuthContext);
    const [search, setSearch] = useState('')
    const [flag, setFlag] = useState(false);
    const { auth } = useFirebase()
    let uid;

    if (user) {
        uid = user.uid;
    }

    const { data, loading: userLoading } = useDoc(`users/${uid}`);
    const isLoaded = (...loaders) => loaders.reduce((condition, loader) => condition || loader, false);

    (data) && console.log(data);


    const { data: events } = useCol('Events');
    const { data: tenders } = useCol('Tenders');

    const toggle = () => setFlag((flag) => !flag);

    const includes = (item, search) => item.name.toLowerCase().includes(search) || item.categories.includes(search);

    if (isLoaded(userLoading)) {
        return (
            <div className='flex-center h-vh-80 bold'>
                <h3 className="font-Raleway">LOADING</h3>
                <div className="loader"></div>
            </div>
        )
    }

    if(data && data.id === "undefined") {
        return (
            <div className='flex-center h-vh-80 bold'>
                <h3>Ta Nevterch orno uu.</h3>
            </div>
        )
    }


    return (
        <div className='container'>
            <Button onClick={() => {auth.signOut()}} >LogOut</Button>
            <SearchInput updateSearch={setSearch} />
            <TenderCampaign toggle={toggle} />
            {flag && <ShowPosts data={events.filter((event) => includes(event, search))} />}
            {!flag && <ShowTender data={tenders.filter((tender) => includes(tender, search))} />}
        </div>
    )
}