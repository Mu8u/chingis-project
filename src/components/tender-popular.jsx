import React, { useState } from 'react';

export const TenderCampaign = ({ toggle }) => {
    const [clicked, setClicked] = useState(false)

    const toggleAnimation = () => {
        if (clicked === false) {
            setClicked(true)
            toggle()
        } else {
            setClicked(false)
            toggle()
        }
    }

    return (
        <div className="flex-col mt-20 h-36">
            <div className="toggle-menu pr w100 b-white flex-row h-36">
                <div className={`flex-center w50 font-Raleway z-i text-center mt-5 ${clicked ? 'c-gray bb-gray6-1' : 'c-secondary bold bb-secondary-2'}`} onClick={toggleAnimation}>Tender</div>
                <div className={`flex-center w50 font-Raleway z-i text-center mt-5 ${clicked ? 'c-secondary bold bb-secondary-2' : 'c-gray bb-gray6-1'}`} onClick={toggleAnimation}>Campaigns</div>
                <div className={`b-secondary h-36 menu-indicator op-little ${clicked ? 'active' : ''}`} />
            </div>
        </div>
    )
}