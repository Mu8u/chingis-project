import React, { useState } from 'react';
import { EventRender } from '../components'
import { SortBy } from './sortby';

export const ShowPosts = ({ data }) => {

    const [sort, setSort] = useState('New');

    return (
        <div>
            <div className="flex-row justify-between items-center w100">
                <div className="font-Raleway bold fs-24 lh-34 mt-15">Posts</div>
                <SortBy type={sort} setType={setSort} />
            </div>
            <div className='flex-col'>
                {sort === 'New' && data.sort((a, b) => a.createdAt > b.createdAt ? -1 : 1).map((e) => <EventRender key={e.id} {...e} />) }
                {sort === 'Popular' && data.sort((a, b) => a.vote > b.vote ? -1 : 1).map((e) => <EventRender key={e.id} {...e} />)}
            </div>
        </div>
    )
}