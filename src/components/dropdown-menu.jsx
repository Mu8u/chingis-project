import React, { useState, useRef } from 'react'
import { Stack, MenuIcon, ProfileIcon, NewProjectIcon, XIcon, HomeIcon } from '../components'
import { DownArrow } from './icons';
import { useHistory } from 'react-router-dom'
import { useOutsideClick } from '../Hooks/use-outside-click'

export const DropdownItem = ({ children, onClick, ...others }) => {

    return (
        <div className='flex-center' onClick={onClick} {...others}>
            {children}
        </div>
    )
}

export const DropdownOptions = ({ children }) => {
    return (
        <Stack className='flex-row absolute t-0 r-0 b-white pa-10 w100' size={5}>
            {children}
        </Stack>
    )
}

export const DropdownOptionsArrow = ({ children }) => {
    return (
        <Stack className='text-center r-0 font-DmSans b-default bradius-10 mb-5' size={2}>
            {children}
        </Stack>
    )
}


export const DropdownMenu = () => {
    const history = useHistory();
    const [show, setShow] = useState(false);

    return (
        <div className='z-i bradius-10'>
            <MenuIcon height={12} width={18} onClick={() => setShow(true)} />
            {show &&
                <DropdownOptions>
                    <XIcon className='l-5 t-15 pa-5' height={12} width={12} onClick={() => setShow(false)} />
                    <DropdownItem onClick={() => { history.push('/addEvent') }}>
                        <NewProjectIcon height={18} width={18} />
                        <div className='ml-10 font-DmSans'>New Project</div>
                    </DropdownItem>
                    <DropdownItem onClick={() => { history.push('/profile') }}>
                        <ProfileIcon height={16} width={14} />
                        <div className='ml-10 font-DmSans'>Profile</div>
                    </DropdownItem>
                    <DropdownItem onClick={() => { history.push('/') }}>
                        <HomeIcon height={19} width={16} />
                        <div className='ml-10 font-DmSans'>Home</div>
                    </DropdownItem>
                </DropdownOptions>}
        </div>
    )
}

export const DropdownMenuArrow = ({ type, setType }) => {
    const [show, setShow] = useState(false);
    const [clicked, setClicked] = useState(false)
    const ref = useRef()

    useOutsideClick(ref, () => { setShow(false) });

    // const checkColor = () => {
    //     if (type === 'New') {
    //         setColor(true)
    //     } else if (type === 'Popular') {
    //         setColor(false)
    //     }
    // }

    return (
        <div className='pr font-Raleway w-150 display-block' ref={ref}>
            {!show && <div className="fs-16 lh-21 text-right mt-12 flex bold justify-end items-center w100" onClick={() => { setShow((show) => !show) }}>
                {type}
                <DownArrow className="ml-12" height={18} width={12} />
            </div>}
            {show &&
                <div className='pr font-Raleway w-150 display-block' ref={ref}>
                    <div className="fs-16 lh-21 text-right mt-12 flex bold justify-end items-center w100" onClick={() => { setShow((show) => !show) }}>
                        <div className='flex items-center'>
                            <div className='fs-16 lh-21 text-right display-block justify-end items-center'>Sort By {type}</div>
                            <DownArrow className="ml-12" height={18} width={12} />
                        </div>
                    </div>
                    <div className='absolute w100'>
                        <DropdownOptionsArrow>
                            <DropdownItem onClick={() => setType('New')}>
                                <div>New</div>
                            </DropdownItem>
                            <div className="b-inactive w100 mb-8 h-1" />
                            <DropdownItem onClick={() => setType('Popular')}>
                                <div>Popular</div>
                            </DropdownItem>
                        </DropdownOptionsArrow>
                    </div>
                </div>}
        </div>
    )
}