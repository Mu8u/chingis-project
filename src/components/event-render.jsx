import React, { useState } from 'react'
import { useStorage, useCol } from '../Hooks';
import { useHistory } from 'react-router-dom'
import { ProgressBar } from './progress-bar'
import moment from 'moment';

export const EventRender = ({ id, name, desc, vote, voteCount, createdAt }) => {
    const history = useHistory();
    const [more, setMore] = useState(false);
    const url = useStorage(`EventImages/${id}/MainImage.jpg`);
    const cdate = (createdAt && createdAt.toDate()) || Date();
    const { data } = useCol(`Events/${id}/Comments/`);

    return (
        <div className='flex-center'>
            <div className="mt-26 mb-10 w100">
                <div className="font-Raleway fs-18 lh-25 h-24 bold">{name}</div>
                <div className='flex items-center justify-center h-200 mt-5 bradius-8'
                    style={{ backgroundColor: "#C0C0C0", backgroundSize: 'cover', backgroundImage: `url("${url}")` }} onClick={() => { history.push(`event-id?id=${id}`) }} />
                <ProgressBar vote={vote} voteCount={voteCount} />
                <div className='flex-row justify-between items-center w100 mt-12'>
                    <div className="font-Raleway fs-14 lh-20 c-inactive">{data.length} Comments</div>
                    <div className="font-Raleway fs-14 lh-20">{vote}/{voteCount} Votes</div>
                </div>
                <div className="wbreak font-Raleway fs-14 lh-21">
                    {
                        desc.length > 150 ?
                        <p className="wbreak">{desc.substring(0, 150)}{!more ? <span className='ul c-primary' onClick={() => { setMore(true) }}><span className="w-20"> </span>SEE MORE</span> : <span onClick={() => { setMore(true) }}>{desc.substring(150, desc.length - 1)}</span>} </p>
                            :
                        <p className="wbreak">{desc}</p>
                    }
                </div>
                <div className="font-Raleway fs-14 lh-21">Posted {moment(cdate).fromNow()}</div>
                <div className="b-inactive w100 mt-20 h-1" />
            </div>
        </div>
    )
}
