import React, { useState } from 'react';
import { TenderRender } from './tender-render';
import { SortBy } from './sortby';

export const ShowTender = ({ data }) => {

    const [sort, setSort] = useState('New');

    return (
        <div>
            <div className="flex-row justify-between items-center w100">
                <div className="font-Raleway fs-24 lh-31 bold mt-15">Posts</div>
                <SortBy type={sort} setType={setSort} />
            </div>
            <div className='flex-col'>
                {sort === 'New' && data.sort((a, b) => a.vote > b.vote ? -1 : 1).map((e) => <TenderRender key={e.id} {...e} />)}
                {sort === 'Popular' &&  data && data.map((e) => <TenderRender key={e.id} {...e} />) }
            </div>
        </div>
    )
}