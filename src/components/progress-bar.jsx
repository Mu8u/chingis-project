import React from 'react';

export const ProgressBar = ({vote, voteCount}) => {
    return (
        <div className='pr'>
            <div className='w100 absolute bradius-10 b-gray4 h-10 mt-2'></div>
            <div className='absolute bradius-10 b-primary h-10 mt-2' style={{ width: `${(vote / voteCount) * 100 || 5}%` }}></div>
        </div>
    )
}