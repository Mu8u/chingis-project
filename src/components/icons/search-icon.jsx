import React from 'react'

export const SearchIcon = ({ height, width, color = "#52575C", ...others }) => {
    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 19 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M17.5353 16.8186L13.6514 13.0396M15.7496 8.13132C15.7496 11.9696 12.5516 15.0811 8.60672 15.0811C4.66183 15.0811 1.46387 11.9696 1.46387 8.13132C1.46387 4.29305 4.66183 1.18152 8.60672 1.18152C12.5516 1.18152 15.7496 4.29305 15.7496 8.13132Z" stroke="#1DD3D2" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            </svg>

        </span>
    )
}
