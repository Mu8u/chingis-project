import React from 'react'

export const HomeIcon = ({ height, width, color = "#00000", ...others }) => {
    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 19 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M6.82101 20.2857V10.9999H12.1782V20.2857M1.46387 8.21423L9.49958 1.71423L17.5353 8.21423V18.4285C17.5353 18.9211 17.3472 19.3934 17.0123 19.7417C16.6774 20.09 16.2232 20.2857 15.7496 20.2857H3.24958C2.77598 20.2857 2.32178 20.09 1.98689 19.7417C1.652 19.3934 1.46387 18.9211 1.46387 18.4285V8.21423Z" stroke="#FCBF49" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>
    )
}