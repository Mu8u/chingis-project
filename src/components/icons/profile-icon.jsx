import React from 'react'

export const ProfileIcon = ({ height, width, color = "#52575C", ...others }) => {
    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 17 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M15.6431 17.5357V15.7499C15.6431 14.8027 15.2669 13.8943 14.5971 13.2246C13.9273 12.5548 13.0189 12.1785 12.0717 12.1785H4.92885C3.98165 12.1785 3.07324 12.5548 2.40347 13.2246C1.7337 13.8943 1.35742 14.8027 1.35742 15.7499V17.5357M12.0717 5.03566C12.0717 7.00811 10.4727 8.60709 8.50028 8.60709C6.52783 8.60709 4.92885 7.00811 4.92885 5.03566C4.92885 3.06322 6.52783 1.46423 8.50028 1.46423C10.4727 1.46423 12.0717 3.06322 12.0717 5.03566Z" stroke="#131415" strokeLinecap="round" strokeLinejoin="round" />
            </svg>

        </span>
    )
}