import React, { useState } from 'react'
import { useStorage } from '../Hooks';
import { useCol } from '../Hooks'
import { useHistory } from 'react-router-dom'

export const TenderRender = ({ id, name, desc }) => {
    const history = useHistory();
    const url = useStorage(`Tenders/Events/${id}/MainImage.jpg`);
    const { data } = useCol(`Tenders/${id}/Comments/`);
    const [more, setMore] = useState(false);
    return (
        <div className='flex-center'>
            <div className="mt-26 mb-10 w100">
                <div className="font-Raleway fs-18 lh-25 h-24 bold">{name}</div>
                <div className='flex items-center justify-center h-200 mt-5 bradius-8'
                    style={{ backgroundColor: "#C0C0C0", backgroundSize: 'cover', backgroundImage: `url("${url}")` }} onClick={() => { history.push(`tender-id?id=${id}`) }} />
                <div className='flex-row justify-between items-center w100 mt-12'>
                    <div className="font-Raleway fs-16 lh-24 c-inactive">{data.length} Comments</div>
                </div>
                <div className="font-Raleway fs-14 lh-21">
                    {
                        desc.length > 150 ?
                            <p className="wbreak">{desc.substring(0, 150)}{!more ? <span className='ul c-primary' onClick={() => { setMore(true) }}>SEE MORE</span> : <span onClick={() => { setMore(true) }}>{desc.substring(150, desc.length - 1)}</span>} </p>
                            :
                            <p className="wbreak">{desc}</p>
                    }
                </div>
                <div className="b-inactive w100 mt-20 h-2" />
            </div>
        </div>
    )
}
