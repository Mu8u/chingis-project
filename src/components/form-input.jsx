import React, { useState } from 'react';
import { EyeOffIcon, LockIcon } from './index';
import { MailIcon, ProfileIcon } from './icons';

export const FormInput = (props) => {
    let { className, label, type, ...others } = props;

    const [inputType, setInputType] = useState(type);

    const changeType = () => setInputType((inputType) => inputType === 'password' ? 'text' : 'password');

    if (type === 'password') {
        return (

            <div className=' flex-col'>
                <div className='pr flex-col'>
                    <input className={`info-input ${className}`} type={inputType} {...others} />
                    <LockIcon height={20} width={18} className='absolute' style={{ left: 10, top: 12 }}></LockIcon>
                    <EyeOffIcon onClick={changeType} height={24} width={24} className='absolute' style={{ right: 10, top: 13 }} />
                </div>
            </div>
        );
    }
    if (type === 'email') {
        return (

            <div className=' flex-col'>
                <div className='pr flex-col'>
                    <input className={`info-input ${className}`} type={inputType} {...others} />
                    <MailIcon height={20} width={18} className='absolute' style={{ left: 10, top: 12 }}></MailIcon>
                    <EyeOffIcon onClick={changeType} height={24} width={24} className='absolute' style={{ right: 10, top: 13 }} />
                </div>
            </div>
        );
    }
    if (type === 'username') {
        return (

            <div className=' flex-col'>
                <div className='pr flex-col'>
                    <input className={`info-input ${className}`} type={inputType} {...others} />
                    <ProfileIcon height={20} width={18} className='absolute' style={{ left: 10, top: 12 }}></ProfileIcon>
                    <EyeOffIcon onClick={changeType} height={24} width={24} className='absolute' style={{ right: 10, top: 13 }} />
                </div>
            </div>
        );
    }
    if (type === 'event') {
        return (
            <div className=' flex-col'>
                <div className='pr pa-16 flex-col'>
                    <textarea className={`info-input lh-25 ${className}`} style={{paddingLeft: 0}} type={inputType} {...others} />
                </div>
            </div>
        );
    }

    return (

        <div className=' flex-col'>
            <div className='pr flex-col'>
                <input className={`info-input ${className}`} type={inputType} {...others} />
            </div>
        </div>

        // <div className='pr flex-col'>
        //     {/* <div className='b-default br-light-gray-1 fs-16 font-comfortaa lh-20 ti bradius-10'></div> */}
        //     {/* <div className='ph-2 fs-16 font-ubuntu pa-12'></div> */}
        //     <div className='pr flex-col'>
        //     <input className={`input ${className}`} type={inputType} {...others} />
        //     {type==='password' && <EyeOffIcon onClick={changeType} height={24} width={24} className='absolute' style={{right: 15, top: 15}} />}
        //     </div>
        // </div>
    );
};