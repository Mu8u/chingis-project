import React from 'react';
import { useDoc, useCol } from '../Hooks/firebase';
import { useState } from 'react';
import { useEffect } from 'react';

export const Upvote = (props) => {
    let { children, disabled, className, upvoted, eventId, date, uid, projectName, ...others } = props;
    let { data: username } = useDoc(`users/${uid}`);
    let { createRecord } = useCol(`Events/${eventId}/votes`);
    let { updateRecord: eventInfo } = useDoc(`Events/${eventId}`);

    let { deleteRecord, data: pr } = useCol(`Events/${eventId}/votes/`);
    let { deleteRecord: deleteRecord2 } = useCol(`users/${uid}/votes`);
    const { data: voter, loading: voteLoading } = useDoc(`users/${uid}/votes/${eventId}`);
    let { createRecord: createRecord2 } = useCol(`users/${uid}/votes`);


    const isLoaded = (...loaders) => loaders.reduce((condition, loader) => condition || loader, false);
    
    const [state, setState] = useState(null);
    let a = voter && voter.projectId === eventId;
    useEffect(() => {
        setState(a);
    }, [a])


    let upvote = () => {
        if (state === false) {
            setState(true);
            createRecord(username && username.id, { createdAt: date, userId: uid, username: username.username, projectId: eventId });
            createRecord2(eventId, { createdAt: date, projectId: eventId, projectName: projectName });
            console.log(pr.length, " <==length")
            eventInfo({'vote': pr.length + 1})
        } else {
            setState(false);
            deleteRecord(username && username.id);
            deleteRecord2(eventId)
            eventInfo({'vote': pr.length - 1})
        }
    }

    if (isLoaded(voteLoading)) {
        return (
            <div className='flex-center h-vh-80 bold'>
                <h3>Loading ...</h3>
                <div className="loader"></div>
            </div>
        )
    }

    return (
        <button onClick={upvote} className={`btn c-white ${className} ${disabled && 'disabled'}`} {...others}>
            {state === true ? "voted" : "vote"}
        </button>
    );
};