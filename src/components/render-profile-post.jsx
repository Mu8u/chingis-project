import React, { useState } from 'react';
import { DotIcon, TrashIcon } from '../components'

export const RenderProfilePost = ({vote, voteCount, name, desc, mainImageUrl }) => {

    const [show, setShow] = useState(false)
    const [more, setMore] = useState(false)

    return (
        <div className='pr mt-20 bb-gray2-1'>
            <div className='flex justify-between items-center'>
                <div className='font-Raleway fs-24 fw-700'>{name}</div>
                <DotIcon className="pa-5" width={25} height={25} onClick={() => { setShow((show) => !show) }} />
            </div>
            <div style={{ backgroundColor: 'lightgray', backgroundSize: 'cover', backgroundPosition: 'center', backgroundRepeat: 'no-repeat', backgroundImage: `url("${mainImageUrl}")` }} className='bradius-10 w100 h30' ></div>
            <div className='pr'>
                <div className='w100 absolute bradius-10 b-gray4 h-10 mt-2'></div>
                <div className='absolute bradius-10 b-primary h-10 mt-2' style={{ width: `${(vote / voteCount) * 100 || 5}%` }}></div>
            </div>
            {
                desc.length > 150 ?
                    <p className="wbreak">{desc.substring(0, 150)}{!more ? <span className='ul c-primary' onClick={() => { setMore(true) }}> ... see more</span> : <span onClick={() => { setMore(true) }}>{desc.substring(150, desc.length - 1)}</span>} </p>
                    :
                    <p className="wbreak">{desc}</p>
            }


            {show &&
                <>
                    <div className='absolute t-45 w-vw-90 bshadow bradius-10 b-white margin-auto h-130 flex-col items-center justify-between'>
                        <div className='w100 h-50 mt-10 text-center'>
                            <div className='w-30 h-2 b-primary margin-auto'></div>
                            <div className='fs-18 bb-gray2-1 pb-10 font-DmSans-'>More</div>
                        </div>
                        {/* <div className='b-gray2 w100 h-1'></div> */}
                        <div className='w100' >
                            <div className='w100 pa-4 ma-12 flex-start flex items-center'>
                                <TrashIcon width={16} height={16} className='pr-10' />
                                <div onClick={'hehe boi'} className='pa-5 fs-16 font-DmSans'>Remove</div>
                            </div>
                        </div>
                    </div>
                    <div className='absolute' style={{top: 25, right: 10}}>
                        <svg width="26" height="23" viewBox="0 0 26 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13 0L25.9904 22.5H0.00961876L13 0Z" fill="#FCFCFD"/>
                        </svg>
                    </div>
                </>}
        </div>
    )
}