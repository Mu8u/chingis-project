import React from 'react';

export const Input = (props) => {
    let { className, inputRef, ...others } = props;

    return (
        <div>
            <input ref={inputRef} className={`input b-gray0 ${className}`} {...others}/>
        </div>
    );
};